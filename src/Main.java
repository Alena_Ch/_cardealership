import java.util.Scanner;

public class Main {

    public static void displayMenu() {
        System.out.println("Вы вошли в меню Автосалона.\nПожалуйста, выберите интересующую Вас информацию:" +
                "\n1 - зарегистрировать новый автомобиль в автосалоне;" +
                "\n2 - получить информацию об имеющихся в продаже моделях;" +
                "\n3 - получить информацию о проданных авто и их покупателях;" +
                "\n4 - получить информацию о клиентах автосалона." +
                "\nВВЕДИТЕ ЧИСЛО:");
    }

    public static void pressButton() {
        CarDealership cd = new CarDealership();
        Clients cl = new Clients();

        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();

        switch (num) {
            case 1:
                cd.registration();
                break;
            case 2:
                cd.getSoldCarInformation();
                break;
            case 3:
                cd.getPurchaseInfo();
                break;
            case 4:
                cl.getCustomersInfo();
                break;
            default:
                System.out.println("Извините, запрашиваемая информация не найдена.\nПроверьте правильность ввода данных!");
        }
    }

    public static void main(String[] args) {

        Main enter = new Main();
        enter.displayMenu();
        enter.pressButton();
        enter.pressButton();
        enter.pressButton();
        enter.pressButton();
        enter.pressButton();

    }

}
