public class Cars {

    //protected static Cars[]cars;

    protected String brand;
    protected String model;
    protected double engSize;
    protected double acceleration;
    protected String typeOfbody;
    protected String yearOfProduction;

    public Cars() {
    }

    public Cars(String brand, String model, double engSize, double acceleration, String typeOfbody, String yearOfProduction) {
        this.brand = brand;
        this.model = model;
        this.engSize = engSize;
        this.acceleration = acceleration;
        this.typeOfbody = typeOfbody;
        this.yearOfProduction = yearOfProduction;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
    public double getEngSize() {
        return engSize;
    }

    public void setEngSize(double engSize) {
        this.engSize = engSize;
    }

    public double getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(double acceleration) {
        this.acceleration = acceleration;
    }

    public String getTypeOfbody() {
        return typeOfbody;
    }

    public void setTypeOfbody(String typeOfbody) {
        this.typeOfbody = typeOfbody;
    }

    public String getYearOfProduction() {
        return yearOfProduction;
    }

    public void setYearOfProduction(String yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
    }

    public String getInfo(){
        return " ";
    }
    public void showRange(){
        System.out.println(" ");
    }
    public void addNewCar(){
        System.out.println(" ");
    }
}
