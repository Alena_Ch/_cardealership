public class Acura extends Cars {

    public Acura(){

    }

    public Acura(String brand, String model, double engSize, double acceleration, String typeOfbody, String yearOfProduction) {
        super(brand, model, engSize, acceleration, typeOfbody, yearOfProduction);
    }

    @Override
    public String getInfo(){
        return  "Авто: "+brand+"\nМодель: "+model+
                "\nОбъем двигателя: "+engSize+
                "\nВремя разгона до 100 км/ч: "+acceleration+", sec " +
                "\nТип кузова: "+typeOfbody+"" +
                "\nГод выпуска: "+yearOfProduction;
    }
}
