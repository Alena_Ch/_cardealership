import java.util.Scanner;

//реализовать здесь методы 1, 2 и 3. В main создать объект класса Дилершип и на нем вызывать методы

public class CarDealership {
    static Scanner sc = new Scanner(System.in);

    public static void registration(){

        System.out.println("Для того, чтобы зарегистрировать Ваш автомобилиь запоните форму ниже");
        System.out.println("Введите Ваш ID: ");
        int id = sc.nextInt();
        System.out.println("Введите марку купленного автомобиля: ");
        sc.next();
        String brand = sc.nextLine();
        System.out.println("Регистрация завершена!");
    }


    public static void getSoldCarInformation() {

        Clients[] clients = new Clients[3];
        clients[0] = new Clients("Ivan", "Petrov", 111, "HYUNDAI Genesis", "08.09.2018");
        clients[1] = new Clients("Petr", "Smirnov", 222, "FORD Focus", "08.09.2018");
        clients[2] = new Clients("Timur", "Ivanov", 333, "SKODA Oktavia", "08.09.2018");

        System.out.println("Информация о проданных сегодня автомобилях:");
        for (int i = 0; i < clients.length; i++) {
            System.out.println("Покупатель: " +clients[i].name+" "+clients[i].surname+". Персональноый ID: "+clients[i].clientsId+". Совершена покупка: "+clients[i].purchase+", "+clients[i].dateOfPurchase);

        }
    }

    public static void getPurchaseInfo() {
        Cars[] cars = new Cars[4];
        cars[0] = new Cars("FORD", "Fusion", 1.6, 11.1, "hatchback", "2008");
        cars[1] = new Cars("ACURA", "MDX", 3.5, 10.0, "SUV", "2013");
        cars[2] = new Cars("HYUNDAI", "Elantra", 1.8, 11.5, "sedan", "2007");
        cars[3] = new Cars("NISSAN", "Almera", 1.8, 13.5, "wagon", "2017");

        System.out.println("В настоящий момент в продаже имеются:");
        for (int i = 0; i < cars.length; i++) {
            System.out.println("Автомобиль "+cars[i].brand+" "+cars[i].model+". "+cars[i].typeOfbody+". Двигатель: "+cars[i].engSize+", разгон за "+cars[i].acceleration+"сек. Год выпуска: "+ cars[i].yearOfProduction);
        }
    }


}
