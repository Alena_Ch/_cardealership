import java.util.ArrayList;

public class Clients {

    protected String name;
    protected String surname;
    protected int clientsId;
    protected String purchase;
    protected String dateOfPurchase;

    public Clients() {
    }

    public Clients(String name, String surname, int clientsId, String purchase, String dateOfPurchase) {
        this.name = name;
        this.surname = surname;
        this.clientsId = clientsId;

        this.purchase = purchase;
        this.dateOfPurchase = dateOfPurchase;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getClientsId() {
        return clientsId;
    }

    public void setClientsId(int clientsId) {
        this.clientsId = clientsId;
    }

    public String getPurchase() {
        return purchase;
    }

    public void setPurchase(String purchase) {
        this.purchase = purchase;
    }

    public String getDateOfPurchase() {
        return dateOfPurchase;
    }

    public void setDateOfPurchase(String dateOfPurchase) {
        this.dateOfPurchase = dateOfPurchase;
    }

    public void getCustomersInfo(){
        System.out.println("Наши клиенты: ");
        ArrayList<String> clients = new ArrayList<>();
        clients.add("Егор Темников");
        clients.add("Ольга Белая");
        clients.add("Вадим Белоусов");
        clients.add("Руслан Архипов");

        for (String s: clients){
            System.out.println(s);
        }
    }






}
