
public class Purchase {


    protected String soldCar;
    protected String buyer;//clientsID
    protected Cars[] cars1; //список проданных авто


    public Purchase() {
    }

    public Purchase(String soldCar, String buyer, Cars[] cars1) {
        this.soldCar = soldCar;
        this.buyer = buyer;
        this.cars1 = cars1;
    }

    public String getSoldCar() {
        return soldCar;
    }

    public void setSoldCar(String soldCar) {
        this.soldCar = soldCar;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public Purchase(Cars[] cars1) {
        this.cars1 = cars1;
    }


}
